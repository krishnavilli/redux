import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import {  createStore,combineReducers } from 'redux';
import { Provider } from 'react-redux';  



const reducers = combineReducers({});

function configureStore(initialState = {}) {  
  const store = createStore(reducers, initialState);
  return store;
};
const store = configureStore();  


ReactDOM.render(
	<Provider store={store}>
    <App />
  </Provider>,
   document.getElementById('root'));

registerServiceWorker();
