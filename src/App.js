import React, { Component } from 'react';
import './App.css';
import About from './about';
import { BrowserRouter as Router, Switch, Route, Link , BrowserRouter } from 'react-router-dom';
import { connect } from 'react-redux';


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img  className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          Welcome To React  World
        </p>	      	  
	   <Router>
			<div>			
				  <nav>
					<Link to="/About">About Us</Link> 
				  </nav>             
				 				 
				 <div>
					  <Route path='/About' component={About} />
				 </div>
			 </div>
		</Router>
		 
		
      <ul>
        <li>Ramamoorthy villi</li>
        <li>{this.props.name}</li>
        <li>Varun</li>
      </ul>
	  </div> 
    );
  }
}



const mapStateToProps = (state, ownProps) => ({  
  name: 'Vimal'
});


const mapDispatchToProps = {  
 
};

const AppContainer = connect(  
  mapStateToProps,
  mapDispatchToProps
)(App);

export default AppContainer;


